# region Project Setup
import pygame
import pygame.math as m
import numpy

pygame.init()
pygame.display.set_caption("Obama Frisbee: Saving America")
# Obama = pygame.image.load("spaceship.png")
screen = pygame.display.set_mode((750, 600))
# endregion

# region Color Palette
# contains color values used in the project

skyBlue = (17, 181, 228)
paleCyan = (165, 255, 214)
red = (219, 48, 105)
black = (54, 57, 59)
yellow = (245, 213, 71)


# endregion

# region CLASSES

class Ball:
    def __init__(self, color, size, pos, vel):
        self.color = color
        self.size = size
        self.pos = pos
        self.vel = vel
        self.terminalVel = -7
        self.gravity = -0.1
        self.f = frisbees[0]
    def Update(self):

        if self.pos.y <= 560:

            if self.vel.y > self.terminalVel:
                self.vel.y += self.gravity


        else:
            self.bounceWall("h")

        if self.pos.x >= 710:
            self.pos.x = 700
            self.bounceWall("v")

        if self.pos.x <= -10:
            self.pos.x = 0
            self.bounceWall("v")

            # collision detection with frisbees
        for p in range(0, len(frisbees) - 1):
            self.f = frisbees[p]
            if self.f.pos.x + (self.f.size.x) > self.pos.x:
                if self.f.pos.x < self.pos.x:

                    if abs(self.f.pos.y - self.pos.y) < self.size.y / 2:
                        # if below frisbee
                        if self.f.pos.y<self.pos.y:
                            if abs(self.f.pos.y - self.pos.y) > self.size.y / 4:
                                self.vel.y = abs(self.vel.y) * -1
                                self.pos.y = self.f.pos.y + (self.f.size.y)
                        # if above frisbee
                        else:
                            self.pos.y = self.f.pos.y - (self.f.size.y)
                            self.vel.y = abs(self.vel.y) *1.1
                        print("bounce from frisbee collision")
                    else:
                        if abs(self.f.pos.y - self.pos.y) < self.size.y / 2 - self.vel.y:
                            # if below frisbee
                            if self.f.pos.y < self.pos.y:
                                if abs(self.f.pos.y - self.pos.y) > self.size.y / 4:
                                    self.vel.y = abs(self.vel.y) * -1
                                    self.pos.y = self.f.pos.y + (self.f.size.y)
                            # if above frisbee
                            else:
                                self.pos.y = self.f.pos.y - (self.f.size.y)
                                self.vel.y = abs(self.vel.y) * 1.1
                            print("bounce from frisbee collision")


            # move from velocity

        self.pos -= self.vel

    def Draw(self):
        pygame.draw.ellipse(screen, (255, 0, 0), (self.pos.x, self.pos.y-50, self.size.x, self.size.y))




    def bounceWall(self, wallType):
        if wallType == "h":

            self.vel.y = -self.vel.y


        if wallType == "v":
            self.vel.x = -self.vel.x


class PlayerSide():
    def __init__(self, color, moveSpeed, pos, size):
        self.color = color
        self.moveSpeed = moveSpeed
        self.pos = pos
        self.size = size
        self.button = False

    def Update(self):
        if keys[pygame.K_UP]:
            self.pos.y -= self.moveSpeed
        if keys[pygame.K_DOWN]:
            self.pos.y += self.moveSpeed
        if keys[pygame.K_SPACE]:
            if self.button == True:
                frisbees.append(Frisbee(self.pos, m.Vector2(10, 0)))
                self.button = False
        else:
            self.button = True



    def Draw(self):
        pygame.draw.rect(screen, self.color, (self.pos.x, self.pos.y, self.size.x, self.size.y))


class TestBallPlayer():
    def __init__(self):
        self.pos = m.Vector2(50, 50)
        self.width = 40
        self.height = 50
        self.moveSpeed = 6
        self.vel = -1
        self.gravity = -0.1
        self.terminalVel = -7

    def Draw(self):
        pygame.draw.ellipse(screen, (255, 0, 0), (self.pos.x, self.pos.y, self.width, self.height))

    def Update(self):
        # accept input
        if keys[pygame.K_LEFT]:
            self.pos.x -= self.moveSpeed
        if keys[pygame.K_RIGHT]:
            self.pos.x += self.moveSpeed
        if keys[pygame.K_UP]:
            self.pos.y -= self.moveSpeed
        if keys[pygame.K_DOWN]:
            self.pos.y += self.moveSpeed

        # gravity
        if self.pos.y <= 300:

            if self.vel > self.terminalVel:
                self.vel += self.gravity
            self.pos.y -= self.vel

        else:
            self.vel = 0


class Frisbee():
    def __init__(self, pos, direction):
        self.color = paleCyan
        self.size = m.Vector2(115, 8)
        self.pos = pos
        self.direction = direction

    def Update(self):
        self.pos = (self.pos + self.direction)
        # if self.pos.x > 500:
        #    frisbees.remove(self)

    def Draw(self):
        pygame.draw.rect(screen, self.color, (self.pos.x, self.pos.y, self.size.x, self.size.y))


class Block():
    pass


# endregion

# region START
# contains all that is necessary to begin the game and put it in its initial state.
# this is where all instantiations of the initial objects belong



player = PlayerSide(skyBlue, 3, m.Vector2(20, 20), m.Vector2(30, 30))
# test = TestBallPlayer()

frisbees = [Frisbee(m.Vector2(), m.Vector2(10, 0))]
bouncyBall = Ball(red, m.Vector2(50, 50), m.Vector2(50, 50), m.Vector2(-3, 0))

# endregion


run = True

while run:
    pygame.time.delay(10)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    # LOOP PAST THIS POINT:

    # region UPDATE
    # a center for all the code that will run each frame. Uses references to update each instance of each class.

    # get input
    keys = pygame.key.get_pressed()

    # update instances
    player.Update()
    bouncyBall.Update()
    for x in range(0, len(frisbees)-1):
        frisbees[x].Update()

        x += 1

    # endregion

    # region DRAW

    screen.fill(black)

    # draw instances
    player.Draw()
    bouncyBall.Draw()
    for x in range(0, len(frisbees)-1):
        frisbees[x].Draw()
        x += 1

    # update screen
    pygame.display.update()

    # endregion

pygame.quit()